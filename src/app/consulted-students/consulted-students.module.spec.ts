import { ConsultedStudentsModule } from './consulted-students.module';
import { TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ConsultedStudentsModule', () => {
  let consultedStudentsModule: ConsultedStudentsModule;

  beforeEach(async() => {
    return TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot()
      ]
    }).compileComponents().then(() => {
      const translateService = TestBed.get(TranslateService);
      consultedStudentsModule = new ConsultedStudentsModule(translateService);
    });
  });

  it('should create an instance', () => {
    expect(consultedStudentsModule).toBeTruthy();
  });
});
