import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@universis/common';
import { ConsultedStudentsHomeComponent } from './components/consulted-students-home/consulted-students-home.component';
import { ConsultedStudentsTableComponent } from './components/consulted-students-table/consulted-students-table.component';
// tslint:disable-next-line:max-line-length
import {ConsultedStudentsTableConfigurationResolver, ConsultedStudentsTableSearchResolver } from './components/consulted-students-table/consulted-students-table-config.resolver';

const routes: Routes = [
  {
    path: '',
    canActivate: [
      AuthGuard
    ],
    component: ConsultedStudentsHomeComponent,
    children: [
        {
            path: '',
            pathMatch: 'full',
            redirectTo: 'list/active'
        },
        {
            path: 'list/:list',
            pathMatch: 'full',
            component: ConsultedStudentsTableComponent,
            resolve: {
                tableConfiguration: ConsultedStudentsTableConfigurationResolver,
                searchConfiguration: ConsultedStudentsTableSearchResolver
            }
        }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultedStudentsRoutingModule { }
